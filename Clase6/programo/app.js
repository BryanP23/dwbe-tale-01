function funcionPrueba(nombre) {
    console.log('check this since a function ' + nombre)
}

funcionPrueba("javaScript");


function saludar (){ // funcion normal
    console.log('salut');
}

let saludar2 = function() { //funcion anonima
    console.log('Hello')
}

let saludar3 = (e) => { 
    e.preventDefault();  //funcion flecha
    console.log("bonjour");
}

let suma4 = (n1, n2) => console.log(n1 +n2);

let elDoble = (n1) => console.log(n1 * 2);

// saludar();
// saludar2();
// saludar3();
// document.write(elDoble(5));
document.getElementById('btn').addEventListener('click', elDoble(6));
